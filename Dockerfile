FROM liliancal/ubuntu-php-apache
RUN apt-get update \
    && apt install -y libgs9-common=9.50~dfsg-5ubuntu4.7 \
    && apt install -y libgs9=9.50~dfsg-5ubuntu4.7 \
    && apt-get clean && rm -rf /var/lib/apt/list
ADD src /var/www
EXPOSE 80
CMD ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]